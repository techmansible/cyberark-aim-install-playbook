CyberArk_AIM_installation
-------------------------

Ansible playbooks for installing CyberArk AIM Agent 

Roles
-----

Below are the role used in this playbook

cyberark-aim-installation -- Installs the CyberArk AIM Agent

Getting Started
----------------

Download required binaries from artifactory
Install CyberArk AIM Agent

Running Playbook
----------------

ansible-playbooks -i inventories/sgrtss/hosts install.yml

